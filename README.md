# CARA DEPLOY PROJECT LARAVEL ATAU LUMEN KE HEROKU

### Jika belum login silahkan login terlebih dahulu :
```
heroku login
```
### Membuka project :
```
cd <project kamu>
```
### Membuat repository :
```
git init
```
### Menambahkan file project pada repository :
```
git add .
```
### Membuat pesan commit :
```
git commit -m "Ini project Laravel"
```
### Membuat file Procfile :
```
echo "web: vendor/bin/heroku-php-apache2 public/" > Procfile
```
### Menambahkan file Procfile pada repository :
```
git add .
```
### Membuat pesan commit :
```
git commit -m "Menambahkan file Procfile"
```
### Membuat aplikasi baru di Heroku :
```
heroku create <nama_project>
```
### Mengatur kunci enkripsi Laravel (Config Vars) :
```
heroku config:set APP_KEY=...
```
### Push project ke Heroku :
```
git push heroku master
```
### Membuka aplikasi yang telah di push tadi :
```
heroku open
```

